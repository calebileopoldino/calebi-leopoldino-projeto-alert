﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Alert_TV.Startup))]
namespace Alert_TV
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
